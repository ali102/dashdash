import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from server import server
from navbar import navbar

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(name='app4',
                server=server, url_base_pathname='/app4/',
                external_stylesheets=[dbc.themes.LUX])

app.layout = html.Div([
    navbar,
    html.Div([html.Label('Dropdown'),
              dcc.Dropdown(
                  options=[
                      {'label': 'New York City', 'value': 'NYC'},
                      {'label': u'Montréal', 'value': 'MTL'},
                      {'label': 'San Francisco', 'value': 'SF'}
                  ],
                  value='MTL'
              ),

              html.Label('Multi-Select Dropdown'),
              dcc.Dropdown(
                  options=[
                      {'label': 'New York City', 'value': 'NYC'},
                      {'label': u'Montréal', 'value': 'MTL'},
                      {'label': 'San Francisco', 'value': 'SF'}
                  ],
                  value=['MTL', 'SF'],
                  multi=True
              ),

              html.Label('Radio Items'),
              dcc.RadioItems(
                  options=[
                      {'label': 'New York City', 'value': 'NYC'},
                      {'label': u'Montréal', 'value': 'MTL'},
                      {'label': 'San Francisco', 'value': 'SF'}
                  ],
                  value='MTL'
              ),

              html.Label('Checkboxes'),
              dcc.Checklist(
                  options=[
                      {'label': 'New York City', 'value': 'NYC'},
                      {'label': u'Montréal', 'value': 'MTL'},
                      {'label': 'San Francisco', 'value': 'SF'}
                  ],
                  value=['MTL', 'SF']
              ),

              html.Label('Text Input'),
              dcc.Input(value='MTL', type='text'),

              html.Label('Slider'),
              dcc.Slider(
                  min=0,
                  max=9,
                  marks={i: 'Label {}'.format(i) if i == 1 else str(i) for i in range(1, 6)},
                  value=5,
              ),
              ], style={'columnCount': 2})

], )
