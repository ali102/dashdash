import dash_bootstrap_components as dbc

navbar = dbc.NavbarSimple(
    children=[dbc.NavLink("App", href="/app", external_link=True),
              dbc.NavLink("App1", href="/app1", external_link=True),
              dbc.NavLink("App2", href="/app2", external_link=True),
              dbc.NavLink("App3", href="/app3", external_link=True),
              dbc.NavLink("App4", href="/app4", external_link=True),
              dbc.NavLink("App5", href="/app5", external_link=True),
              dbc.NavLink("App6", href="/app6", external_link=True),
              dbc.NavLink("App7", href="/call_back", external_link=True),

              dbc.DropdownMenu(children=[
                  dbc.DropdownMenuItem(children=["Weather prediction",
                                                 dbc.NavLink(href="/app2", external_link=True)]),
                  dbc.DropdownMenuItem(children=["Customer tests",
                                                 dbc.NavLink(href="/uxtest", external_link=True)]),
                  dbc.DropdownMenuItem("Weather prediction", href="/app", external_link=True),
                  dbc.DropdownMenuItem("Customer tests", href="/", external_link=True),
                  dbc.DropdownMenuItem("W1", external_link=True, href="/app1/"),
                  dbc.DropdownMenuItem("W2", href="/app2/", external_link=True),
              ],
                  label="WEATHER DASHBOARDS",
                  loading_state=True,
                  nav=True,
                  in_navbar=True,
              ),
              ],
    brand="VanBoven Dashboards",
    brand_href="/",
    brand_external_link=True,
    color="primary",
    dark=True,
)
