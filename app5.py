import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
from dash.dependencies import Input, Output
from server import server
from navbar import navbar


df = pd.read_csv("intro_bees.csv")
df = df.groupby([
    'State', 'ANSI', 'Affected by',
    'Year', 'state_code'
])[['Pct of Colonies Impacted']].mean()

df.reset_index(inplace=True)
print(df[:5])

app = dash.Dash(name='app5',
                server=server, url_base_pathname='/app5/',
                external_stylesheets=[dbc.themes.LUX])

app.layout = html.Div(
    [
        navbar,
        html.Div(html.H1("Web Application Dashboards with Dash", style={'text-align': 'center'})),
        dcc.Dropdown(id="slct_year",
                     options=[
                         {"label": "2015", "value": 2015},
                         {"label": "2016", "value": 2016},
                         {"label": "2017", "value": 2017},
                         {"label": "2018", "value": 2018}],
                     multi=False,
                     value=2015,
                     style={'width': "40%"}
                     ),

        html.Div(id='output_container', children=[]),
        html.Br(),

        dcc.Graph(id='my_bee_map', figure={})

    ])

# Connect the Plotly graphs with Dash Components
@app.callback(
    [Output(component_id='output_container', component_property='children'),
     Output(component_id='my_bee_map', component_property='figure')],
    [Input(component_id='slct_year', component_property='value')]
)
def update_graph(option_slctd):
    print(option_slctd)
    print(type(option_slctd))

    container = "The year chosen by user was: {}".format(option_slctd)

    # We made a copy so we don't update the data by itself.
    dff = df.copy()
    dff = dff[dff["Year"] == option_slctd]
    dff = dff[dff["Affected by"] == "Varroa_mites"]

    # Plotly Express
    fig = px.choropleth(
        data_frame=dff,
        locationmode='USA-states',
        locations='state_code',
        scope="usa",
        color='Pct of Colonies Impacted',
        hover_data=['State', 'Pct of Colonies Impacted'],
        color_continuous_scale=px.colors.sequential.YlOrRd,
        labels={'Pct of Colonies Impacted': '% of Bee Colonies'},
        template='plotly_dark'
    )

    fig1 = px.bar(
        dff, x='State', y='Pct of Colonies Impacted'
    )

    # Using line chart
    fig2 = px.line(df, x="Year", y="Pct of Colonies Impacted", color="State")

    # Plotly Graph Objects (GO)
    # fig = go.Figure(
    #     data=[go.Choropleth(
    #         locationmode='USA-states',
    #         locations=dff['state_code'],
    #         z=dff["Pct of Colonies Impacted"].astype(float),
    #         colorscale='Reds',
    #     )]
    # )
    #
    # fig.update_layout(
    #     title_text="Bees Affected by Mites in the USA",
    #     title_xanchor="center",
    #     title_font=dict(size=24),
    #     title_x=0.5,
    #     geo=dict(scope='usa'),
    # )

    return container, fig1
