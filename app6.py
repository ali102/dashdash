import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
from dash.dependencies import Input, Output
from server import server
from navbar import navbar

df = pd.read_csv("intro_bees.csv")
df = df.groupby([
    'State', 'ANSI', 'Affected by',
    'Year', 'state_code'
])[['Pct of Colonies Impacted']].mean()

df.reset_index(inplace=True)

app = dash.Dash(name='app6',
                server=server, url_base_pathname='/app6/',
                external_stylesheets=[dbc.themes.LUX])

app.layout = html.Div(
    [
        navbar,
        html.Div(html.H1("Web Application Dashboards with Dash", style={'text-align': 'center'})),
        dcc.Dropdown(id="slct_option",
                     options=[
                         {"label": "Pesticides", "value": "Pesticides"},
                         {"label": "Pests_excl_Varroa", "value": "Pests_excl_Varroa"},
                         {"label": "Varroa_mites", "value": "Varroa_mites"},
                         {"label": "Disease", "value": "Disease"},
                         {"label": "Unknown", "value": "Unknown"},
                         {"label": "Other", "value": "Other"}],
                     multi=False,
                     value="Unknown",
                     style={'width': "40%"}
                     ),

        html.Div(id='output_container', children=[]),
        html.Br(),

        dcc.Graph(id='my_bee_map', figure={})
    ])


# Connect the Plotly graphs with Dash Components
@app.callback(
    [Output(component_id='output_container', component_property='children'),
     Output(component_id='my_bee_map', component_property='figure')],
    [Input(component_id='slct_option', component_property='value')]
)
def update_graph(option_slctd):
    print(option_slctd)
    print(type(option_slctd))

    container = "The reason chosen by user was: {}".format(option_slctd)

    # We made a copy so we don't update the data by itself.
    dff = df.copy()
    dff = dff[dff["Affected by"] == option_slctd]
    dff = dff[(dff["State"] == "California") | (dff["State"] == "Arizona") | (dff["State"] == "Arkansas") |
              (dff["State"] == "Alabama") | (dff["State"] == "Connecticut")]

    fig1 = px.bar(
        dff, x='State', y='Pct of Colonies Impacted'
    )

    # Using line chart
    fig2 = px.line(dff, x="Year", y="Pct of Colonies Impacted", color="State", template="plotly_dark")

    return container, fig2
