import dash
import dash_html_components as html
import plotly.express as px
import pandas as pd
import dash_bootstrap_components as dbc
from server import server
from navbar import navbar

# load the dash app
app = dash.Dash(name='main',
                server=server, url_base_pathname='/',
                external_stylesheets=[dbc.themes.LUX])

app.layout = html.Div(children=[
    navbar,
    html.H1(children='Van Boven Drones', style={
        'textAlign': 'center',
        'color': '#7FDBFF'
    }),

    html.H2(children='''
        We fly together for a better vision and a brighter future!
    ''', style={
        'textAlign': 'center',
        'color': '#7FDBFF'
    }),

])
